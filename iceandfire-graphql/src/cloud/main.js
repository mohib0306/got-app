import express from "express";
import graphqlHTTP from "express-graphql";
import cors from "cors";
import schema from "../schema";

const app = express();
app.use(cors());
app.use(
  "/graphql",
  graphqlHTTP({
    schema,
    graphiql: true,
    pretty: true
  })
);
app.listen(5999, () => {
  console.log("Listening on port 5999. http://localhost:5999");
});
