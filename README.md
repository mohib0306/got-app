# A react app to list GoT houses

## Getting Started

You should run both api and the frontend to test the app

1. To start the api, navigate to `iceandfire-graphql` folder. `yarn install` and then `yarn start`
2. To start the frontend application, navigate to `iceandfire-frontend` folder. `yarn install` and then `yarn start`
