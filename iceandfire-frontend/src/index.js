import React from "react";
import ReactDOM from "react-dom";
import { ApolloClient } from "apollo-client";
import { ThemeProvider } from "@material-ui/core/styles";
import CssBaseline from "@material-ui/core/CssBaseline";
import { InMemoryCache } from "apollo-cache-inmemory";
import { HttpLink } from "apollo-link-http";
import { ApolloProvider } from "react-apollo";
import { Router } from "react-router";
import { createBrowserHistory } from "history";
import theme from "./theme";
import App from "./App";

const history = createBrowserHistory();

const cache = new InMemoryCache();
const client = new ApolloClient({
  cache,
  onError: e => {
    console.log(e);
  },
  link: new HttpLink({
    uri: "http://localhost:5999/graphql"
  })
});

const Root = () => {
  return (
    <ApolloProvider client={client}>
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <Router history={history}>
          <App />
        </Router>
      </ThemeProvider>
    </ApolloProvider>
  );
};

ReactDOM.render(<Root />, document.getElementById("root"));
