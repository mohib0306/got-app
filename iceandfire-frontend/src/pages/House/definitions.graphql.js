import gql from "graphql-tag";

export const HouseQuery = gql`
  query houseQuery($id: ID!) {
    house(id: $id) {
      id
      name
      region
      coatOfArms
      words
      titles
      seats
      currentLord {
        id
        name
      }
      heir {
        id
        name
      }
      overlord {
        id
        name
      }
      founded
      founder {
        id
        name
      }
      memberConnection {
        swornMembers {
          name
        }
      }
    }
  }
`;
