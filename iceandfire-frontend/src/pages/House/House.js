import React from "react";
import Container from "@material-ui/core/Container";
import Typography from "@material-ui/core/Typography";
import CircularProgress from "@material-ui/core/CircularProgress";
import Grid from "@material-ui/core/Grid";
import { graphql } from "react-apollo";
import { HouseQuery } from "./definitions.graphql";

const House = ({ data }) => {
  const { error, house, loading } = data || {};

  if (loading || error) {
    return (
      <Container maxWidth="sm">
        <CircularProgress />{" "}
      </Container>
    );
  }
  return (
    <Container>
      <Grid key={house.id} item xs={12}>
        <Typography variant="h2" component="h1" gutterBottom>
          Game of thrones
        </Typography>
        <br />
        <br />
        <Typography variant="h3">{house.name}</Typography>
        <br />
        <Typography variant="h5">
          Founder: <i>{house.founder ? house.founder.name : "unknown"}</i>
        </Typography>
        <Typography variant="h5">
          Founded: <i>{house.founded ? house.founded : "unknown"}</i>
        </Typography>
        <Typography variant="h5">
          Current Loard:{" "}
          <i>{house.currentLoard ? house.currentLoard.name : "unkonwn"}</i>
        </Typography>
        <Typography variant="h5">
          Heir: <i>{house.heir ? house.heir.name : "unkonwn"}</i>
        </Typography>
        <Typography variant="h5">
          Region: <i>{house.region}</i>
        </Typography>
        <Typography variant="h5">
          Coats of arms:{" "}
          <i>{house.coatOfArms ? house.coatOfArms : "unkonwn"}</i>
        </Typography>
        <Typography variant="h5">
          Words: <i>{house.words ? house.words : "unkonwn"}</i>
        </Typography>
        <Typography variant="h5">
          Titles:{" "}
          <i>
            {house.titles && house.titles.length > 0
              ? house.titles.join("--- ")
              : "no titles achieved"}
          </i>
        </Typography>
        <Typography variant="h5">
          Seats:{" "}
          <i>
            {house.seats && house.seats.length > 0
              ? house.seats.join("--- ")
              : "no seat gained"}
          </i>
        </Typography>
        <Typography variant="h5">
          Sworn Members:{" "}
          <i>
            {house.memberConnection &&
            house.memberConnection.swornMembers.length > 0
              ? house.memberConnection.swornMembers
                  .map(member => member.name)
                  .join(" --- ")
              : "no sworn members"}
          </i>
        </Typography>
      </Grid>
    </Container>
  );
};

export default graphql(HouseQuery, {
  options: props => ({
    variables: { id: props.match.params.id }
  })
})(House);
