import { makeStyles } from "@material-ui/core/styles";

const mainStyles = makeStyles(theme => ({
  cardList: {
    display: "flex",
    flexWrap: "wrap"
    // backgroundColor: "blue",
    // // Match [0, md + 1[
    // //       [0, lg[
    // //       [0, 1280px[
    // [theme.breakpoints.down("md")]: {
    //   backgroundColor: "red"
    // }
  },
  spacedCard: {
    margin: "0px 20px 20px 0px",
    flexBasis: "30%",
    flexGrow: 0,
    maxWidth: "none",
    [theme.breakpoints.down("sm")]: {
      flexBasis: "100%"
    }
  }
}));

export default mainStyles;
