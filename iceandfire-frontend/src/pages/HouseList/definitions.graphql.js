import gql from "graphql-tag";

// export const addCompanyMutation = gql`
//   mutation addCompanyMutation($company: AddCompanyInput!) {
//     addCompany(company: $company) {
//       id
//       name
//       description
//     }
//   }
// `;

export const AllHousesQuery = gql`
  query allHousesQuery {
    allHouses {
      houses {
        id
        name
        founded
        region
        titles
        founder {
          id
          name
        }
        currentLord {
          id
          name
        }
        heir {
          id
          name
        }
        seats
        ancestralWeapons
        diedOut
        memberConnection {
          swornMembers {
            name
          }
        }
      }
    }
  }
`;

// export const addReviewMutation = gql`
//   mutation addReviewMutation($company: ID!, $content: String!) {
//     addReview(company: $company, content: $content) {
//       id
//       content
//     }
//   }
// `;
// export const rateReviewMutation = gql`
//   mutation rateReviewMutation($review: ID!, $like: Boolean!) {
//     rateReview(review: $review, like: $like) {
//       id
//       content
//       likes: ratings(like: true)
//       dislikes: ratings(like: false)
//       allRatings: ratings
//     }
//   }
// `;
// name
//       founded
//       region
//       titles
//       founder {
//         id
//         name
//       }
//       currentLord {
//         id
//         name
//       }
//       heir {
//         id
//         name
//       }
//       seats
//       ancestralWeapons
//       diedOut
//       memberConnection {
//         swornMembers {
//           name
//         }
//       }
//     }
