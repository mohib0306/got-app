import React from "react";
import { useQuery } from "@apollo/react-hooks";
import { withRouter } from "react-router-dom";
import Container from "@material-ui/core/Container";
import Typography from "@material-ui/core/Typography";
import CircularProgress from "@material-ui/core/CircularProgress";
import Grid from "@material-ui/core/Grid";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Button from "@material-ui/core/Button";
import { AllHousesQuery } from "./definitions.graphql";
import mainStyles from "./styles";

const HouseList = ({ history }) => {
  const { loading, error, data } = useQuery(AllHousesQuery);
  const classes = mainStyles();
  if (loading || error) {
    return (
      <Container maxWidth="sm">
        <CircularProgress />{" "}
      </Container>
    );
  }
  const { houses } = data.allHouses || {};

  return (
    <Container>
      <Grid item xs={12}>
        <Typography variant="h2" component="h1" gutterBottom>
          Game of thrones
        </Typography>
        <br />
        <br />
        <Typography variant="h4" component="h1" gutterBottom>
          Houses
        </Typography>
        <div className={classes.cardList}>
          {houses
            ? houses.map(house => {
                return (
                  <Grid
                    className={classes.spacedCard}
                    item
                    xs={12}
                    sm={6}
                    lg={4}
                    xl={3}
                    key={house.id}
                  >
                    <Card raised={false}>
                      <CardContent>
                        <Typography color="textSecondary" gutterBottom>
                          {house.region}
                        </Typography>
                        <Typography variant="h5" component="h2">
                          {house.name}
                        </Typography>
                        <Typography color="textSecondary">
                          Current Lord
                        </Typography>
                        <Typography variant="body2" component="p">
                          {house.currentLord
                            ? house.currentLord.name
                            : "unknown"}
                        </Typography>
                        <Typography color="textSecondary">Founded</Typography>
                        <Typography variant="body2" component="p">
                          {house.founded ? house.founded : "unknown"}
                        </Typography>
                      </CardContent>
                      <CardActions>
                        <Button
                          size="small"
                          onClick={() => history.push(`houses/${house.id}`)}
                        >
                          Learn More
                        </Button>
                      </CardActions>
                    </Card>
                  </Grid>
                );
              })
            : "The show has ended. Get a life man."}
        </div>
      </Grid>
    </Container>
  );
};

export default withRouter(HouseList);
