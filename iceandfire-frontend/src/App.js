import React from "react";
import { Route, Switch } from "react-router";
import HouseList from "./pages/HouseList";
import House from "./pages/House";
export default () => {
  return (
    <Switch>
      <Route path="/houses/:id" component={House} />
      <Route path="/" component={HouseList} />
    </Switch>
  );
};
